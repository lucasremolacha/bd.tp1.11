package tp.main;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
	static RoundRobin rr = new RoundRobin();

	public static void main(String[] args) {
		String o;
		Scanner op = new Scanner(System.in);
		menu();
		o=op.nextLine();
		while(o != "0") {
			switch(o) {
			case "1":menuIngreso();break;
			case "2":listarProcesos();break;
			case "3":comenzar(); break;
			case "4": listarTiempoFinal();break;
			case "5": tiempEsperaPromedio();break;
			}
		menu();
		o=op.nextLine();
		}

	}
	
	
	public static void menu() {
		System.out.println("-----------------------------");
		System.out.println("1-Ingresar proceso");
		System.out.println("2-Listar procesos");
		System.out.println("3-Start");
		System.out.println("4-Listar Tiempo final");
		System.out.println("5-Calcular tiempo promedio");
		System.out.println("-----------------------------");
	}
	
	public static void menuIngreso() {
		Scanner op = new Scanner(System.in);
		System.out.println("Ingrese nombre del proceso:");
		String auxNombre = op.nextLine();
		System.out.println("Ingrese tiempo de servicio:");
		int auxTiempo =Integer.parseInt(op.nextLine());
		rr.agregarProceso(new Proceso(auxNombre, auxTiempo));
		System.out.println("----------------------------------------------------");
		System.out.println("El proceso: "+ auxNombre + " De tiempo: "+ auxTiempo+ " /Fue agregado con exito");
	}
	
	public static void listarProcesos() {
		System.out.println("-----------------------------");
		ArrayList<Proceso> aux= rr.getListProcess();
		for (Proceso p :aux) {
			System.out.println(p.toString());
		}
		System.out.println("-----------------------------");
	}
	
	public static void comenzar() {
		rr.start();
		System.out.println("------------------------------------");
		System.out.println("Round-Robin fue ejecutado con exito");
		System.out.println("------------------------------------");
	}
	
	public static void listarTiempoFinal() {
		System.out.println("-----------------------------");
		ArrayList<Proceso> aux= rr.getListProcess();
		for (Proceso p :aux) {
			System.out.println(p.listTiempoFinal());
		}
		System.out.println("-----------------------------");
	}
	
	public static void tiempEsperaPromedio() {
		System.out.println("El tiempo de espera promedio es: " + rr.calcularTiempoMedioEspera());
	}

}

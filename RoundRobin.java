package tp.main;

import java.util.ArrayList;

public class RoundRobin {
	private ArrayList<Proceso> listProcess = new ArrayList<Proceso>();
	private int q = 10;
	
	public void agregarProceso(Proceso p) {
		listProcess.add(p);
	}
	
	public ArrayList<Proceso> getListProcess (){
		return listProcess;
	}
	
	public void start() {
		boolean corte=true;
		int tiempo=0;
		boolean test= true;
		int auxTS;
		
		while (corte == true){
			for(Proceso p:listProcess) {
				if(p.isEstado()== true){
					auxTS = p.getTiempoServicio()-q;
					if(auxTS <=0) {
						tiempo = tiempo + p.getTiempoServicio();
						p.setTiempoFinal(tiempo);
						p.setEstado(false);
					}else {
						System.out.println("positivo");
						p.setTiempoServicio(p.getTiempoServicio()-q);
						tiempo= tiempo + q;
						test=false;
						System.out.println(tiempo);
					}
				}
			}
			
			if (test==false) {
				test=true;
			}else {corte=false;}
		}
	}
	
	
	public float calcularTiempoMedioEspera() {
		float acum=0;
		for (Proceso p : listProcess) {
			acum = acum + p.getTiempoFinal()- p.getTiempoServicio2();
		}
		return (acum / listProcess.size());
	}
}

package tp.main;

public class Proceso {
	private String nombre;
	private int tiempoServicio;
	private int tiempoServicio2;
	private int tiempoFinal;
	private int tiempoLLegada=0;
	private boolean estado;

	public  Proceso(String nombre, int tiempoServicio) {
		this.nombre= nombre;
		this.tiempoServicio= tiempoServicio;
		this.tiempoServicio2= tiempoServicio;
		this.tiempoFinal=0;
		this.estado= true;
	}
	
	public String toString() {
		return "{ Proceso: " + nombre + " TiempoServicio: "+ tiempoServicio+ " }";
	}
	
	public String listTiempoFinal() {
		return  "{ Proceso: " + nombre + " TiempoFinal: "+ tiempoFinal+ " }";
	}
//////////////////////////////////////////////////////////////////////////////
	
	public int getTiempoFinal() {
		return tiempoFinal;
	}
	public void setTiempoFinal(int tiempoFinal) {
		this.tiempoFinal = tiempoFinal;
	}
	public int getTiempoLLegada() {
		return tiempoLLegada;
	}
	public void setTiempoLLegada(int tiempoLLegada) {
		this.tiempoLLegada = tiempoLLegada;
	}
	public boolean isEstado() {
		return estado;
	}
	public void setEstado(boolean estado) {
		this.estado = estado;
	}
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getTiempoServicio() {
		return tiempoServicio;
	}
	public int getTiempoServicio2() {
		return tiempoServicio2;
	}

	public void setTiempoServicio(int tiempoServicio) {
		this.tiempoServicio = tiempoServicio;
	}
	
	
}
